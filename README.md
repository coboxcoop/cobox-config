# cobox-config

Stores and retrieves a YAML configuration file for use with the cobox stack. Also loads up master key to generate a global identity.

## Example

```js
const Config = require('cobox-config')
const storage = './storage'

const config = Config(storage)
```

## API

```
config.save()
```

Write to YAML storage, path set using the storage path when initialising. Make sure you call this otherwise the config will not write to disk.

```
config.load()
```

Load from YAML, path set using the storage path when initialising. This is called automatically when initialising a `Config`.

```
var key = space.name || space.address || space.address.toString('hex')
config.spaces.get(key)
```

Get a space from the config

```
var key = space.name || space.address || space.address.toString('hex')
config.spaces.set(key, { name, address, encryptionKey })
```

Add a space to the config

```
var key = space.name || space.address || space.address.toString('hex')
config.spaces.delete(key)
```

Remove a space from the config

```
config.spaces.list()
```

List saved spaces
