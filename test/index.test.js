const { describe } = require('tape-plus')
const Config = require('../')
const crypto = require('cobox-crypto')
const fs = require('fs')
const path = require('path')
const yaml = require('js-yaml')
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const mkdirp = require('mkdirp')

const { tmp, cleanup } = require('./util')

describe('load', (context) => {
  context('default', (assert, next) => {
    var storage = tmp()
    var config = Config(storage)
    assert.ok(config.spaces.list() instanceof Array, 'spaces uses KeyHandler')
    assert.ok(config.replicators.list() instanceof Array, 'replicators uses KeyHandler')
    assert.ok(config.devices.list() instanceof Array, 'devices uses KeyHandler')
    cleanup(storage, next)
  })
})

// describe('parent_key', (context) => {
//   context('constructor() - generates and saves new parent_key', (assert, next) => {
//     var storage = tmp()
//     var spy = sinon.spy(crypto)
//     var saveStub = sinon.stub().returns(true)

//     var ProxyConfig = proxyquire('../', {
//       'cobox-crypto': spy,
//       './lib/keys': { saveParentKey: saveStub }
//     })

//     var config = ProxyConfig(storage)

//     assert.ok(config, 'config created')

//     config.deriveKeyPair(1, 'hello')

//     assert.ok(spy.masterKey.calledOnce, 'calls crypto.masterKey()')
//     assert.ok(saveStub.calledOnce, 'saves parent key')

//     cleanup(storage, next)
//   })

//   context('constructor() - loads parent_key from path', (assert, next) => {
//     var storage = tmp()
//     var address = crypto.address()
//     var parentKey = crypto.masterKey()

//     var keyPath = path.join(storage, 'parent_key')
//     mkdirp.sync(path.dirname(keyPath))
//     fs.writeFileSync(keyPath, parentKey, { mode: fs.constants.S_IRUSR })

//     var loadStub = sinon.stub().returns(parentKey)
//     var saveStub = sinon.stub().returns(true)

//     var ProxyConfig = proxyquire('../', { './lib/keys': {
//       loadParentKey: loadStub,
//       saveParentKey: saveStub
//     }
//     })

//     var config = ProxyConfig(storage, address)
//     config.deriveKeyPair(1, 'hello')

//     assert.ok(loadStub.calledWith(storage), 'key is loaded')
//     assert.ok(saveStub.calledWith(storage, parentKey), 'key is saved')
//     cleanup(storage, next)
//   })
// })

describe('options', (context) => {
  context('reload', (assert, next) => {
    var storage = tmp()
    var config = Config(storage, { hello: true })
    assert.notOk(config.options.get('hello'), 'options must be manually set')
    cleanup(storage, next)
  })
})
