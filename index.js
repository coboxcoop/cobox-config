const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')
const os = require('os')
const mkdirp = require('mkdirp')
const constants = require('cobox-constants')

const CONFIG_FILE = 'config.yml'

module.exports = (storage, opts) => new CoBoxConfig(storage, opts)

const KeyHandler = require('./lib/handlers/keys')
const MapHandler = require('./lib/handlers/map')

const defaultConfig = (options = {}) => ({
  options,
  spaces: { byKey: {}, byName: {} },
  replicators: { byKey: {}, byName: {} },
  devices: { byKey: {}, byName: {} }
})

class CoBoxConfig {
  constructor (storage, opts = {}) {
    this.root = storage || constants.storage
    this.storage = path.join(this.root, CONFIG_FILE)
    this._opts = opts

    mkdirp.sync(path.join(this.root, 'logs'))

    var config = Object.assign(defaultConfig())

    if (!fs.existsSync(this.storage)) {
      fs.writeFileSync(this.storage, yaml.safeDump(defaultConfig(), { sortKeys: true }))
      this._spaces = config.spaces
      this._replicators = config.replicators
      this._devices = config.devices
      this._options = config.options
    } else {
      this.load()
    }

    this.spaces = KeyHandler(this._spaces)
    this.replicators = KeyHandler(this._replicators)
    this.devices = KeyHandler(this._devices)
    this.options = MapHandler(this._options)
  }

  save () {
    try {
      var config = defaultConfig()
      config.spaces = this._spaces
      config.replicators = this._replicators
      config.devices = this._devices
      config.options = this._options

      fs.writeFileSync(this.storage, yaml.safeDump(config, { sortKeys: true }))
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }

  load () {
    try {
      const config = yaml.safeLoad(fs.readFileSync(this.storage, 'utf8'))
      this._spaces = config.spaces
      this._replicators = config.replicators
      this._devices = config.devices
      this._options = config.options
      return true
    } catch (err) {
      console.error(err)
      return false
    }
  }
}
