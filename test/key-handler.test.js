const { describe } = require('tape-plus')
const Config = require('../')
const crypto = require('cobox-crypto')
const fs = require('fs')
const path = require('path')
const yaml = require('js-yaml')
const randomWords = require('random-words')

const { tmp, cleanup } = require('./util')

describe('set', (context) => {
  var storage

  context.beforeEach((c) => {
    storage = tmp()
  })

  context.afterEach((c) => {
    cleanup(storage)
  })

  context('byKey', (assert, next) => {
    var config = Config(storage)
    var space = {
      address: crypto.address(),
      name: randomWords(1).pop()
    }

    config.spaces.set(space.address, space)

    config.save()

    var reload = yaml.safeLoad(fs.readFileSync(path.join(storage, 'config.yml')))
    var rawSpace = reload.spaces.byKey[space.address.toString('hex')]

    assert.same(config.spaces.get(space.address), rawSpace, 'saves spaces to storage')
    next()
  })

  context('byName', (assert, next) => {
    var config = Config(storage)
    var space = Object.assign({ name: 'space-a' }, crypto.unpack(crypto.accessKey()))

    config.spaces.set(space.address, space)

    config.save()

    var reload = yaml.safeLoad(fs.readFileSync(path.join(storage, 'config.yml')))
    var rawSpace = reload.spaces.byKey[space.address.toString('hex')]

    assert.ok(rawSpace.name, 'saves space with a name')
    assert.same(config.spaces.get(space.name), rawSpace, 'gets the space using a name')

    var reload = Config(storage)

    assert.same(reload._spaces, config._spaces, 'load spaces from storage')
    next()
  })
})

describe('get', (context) => {
  var storage

  context.beforeEach((c) => {
    storage = tmp()
  })

  context.afterEach((c) => {
    cleanup(storage)
  })

  context('works with buffers or strings', (assert, next) => {
    var config = Config(storage)
    var space = crypto.unpack(crypto.accessKey())

    config.spaces.set(space.address, space)

    config.save()

    assert.ok(config.spaces.get(space.address), 'get space works with a buffer')
    assert.ok(config.spaces.get(space.address.toString('hex')), 'get space works with a hex')

    next()
  })
})

describe('del', (context) => {
  var storage

  context.beforeEach((c) => {
    storage = tmp()
  })

  context.afterEach((c) => {
    cleanup(storage)
  })

  context('deletes relevant entry using buffers', (assert, next) => {
    var config = Config(storage)
    var space = crypto.unpack(crypto.accessKey())

    config.spaces.set(space.address, space)

    config.save()
    config.load()

    config.spaces.delete(space.address)

    assert.same(config.spaces.list(), [], 'deleted the space')
    next()
  })

  context('deletes relevant entry using buffers', (assert, next) => {
    var config = Config(storage)
    var space = crypto.unpack(crypto.accessKey())

    config.spaces.set(space.address, space)

    config.save()
    config.load()

    config.spaces.delete(space.address.toString('hex'))

    assert.same(config.spaces.list(), [], 'deleted the space')
    next()
  })

  context('deletes relevant entry using names', (assert, next) => {
    var config = Config(storage)
    var space = Object.assign({ name: 'space-a' }, crypto.unpack(crypto.accessKey()))

    config.spaces.set(space.address, space)

    config.save()
    config.load()

    assert.same(config.spaces.list().length, 1, 'saved the space')

    config.spaces.delete(space.name)

    config.save()
    config.load()

    assert.same(config.spaces.list(), [], 'deleted the space')
    next()
  })
})
