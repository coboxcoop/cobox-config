function isPubKey (variable) {
  if (!(typeof variable === 'string' || variable instanceof Buffer)) return false
  return Buffer.from(variable, 'hex').length === 32
}

function isName (variable) {
  return typeof variable === 'string'
}

module.exports = { isPubKey, isName }
