module.exports = function MapHandler (collection) {
  this.collection = collection
  this.map = new Map(Object.keys(this.collection).map((k, i) => [k, Object.values(this.collection)[i]]))
  return {
    get: (id) => this.map.get(id),
    set: (id, entry) => this.map.set(id, entry) && (this.collection[id] = entry),
    delete: (id) => this.map.delete(id) && (delete this.collection[id])
  }
}
