const crypto = require('cobox-crypto')
const { isPubKey, isName } = require('../../util')

const KEY_NAMES = ['name', 'address', 'encryptionKey']

const KeyHandler = module.exports = (collection) => ({
  get: (id) => {
    if (crypto.isKey(id)) return collection.byKey[id.toString('hex')]
    if (isName(id)) return collection.byName[id]
    return false
  },
  set: (id, entry) => {
    if (!isValid(entry)) return false
    if (!entry) return false
    var clone = Object.assign({}, entry)
    hexify(clone)
    compact(clone)
    Object.keys(clone).forEach((k) => {
      if (crypto.isKey(clone[k])) clone[k] = clone[k].toString('hex')
    })
    if (crypto.isKey(clone.address)) collection.byKey[clone.address.toString('hex')] = clone
    if (isName(clone.name)) collection.byName[clone.name] = clone
    return true
  },
  delete: (id) => {
    if (crypto.isKey(id)) {
      entry = collection.byKey[id.toString('hex')]
      if (!entry) return false
      if (entry.name) delete collection.byName[entry.name]
      return delete collection.byKey[entry.address.toString('hex')]
    } else if (isName(id)) {
      entry = collection.byName[id]
      if (!entry) return false
      return delete collection.byName[id] && delete collection.byKey[entry.address.toString('hex')]
    }
  },
  list: () => Object.values(collection.byKey)
})

function hexify (obj) {
  Object.keys(obj).forEach((key) => {
    if (crypto.isKey(obj[key])) obj[key] = obj[key].toString('hex')
  })
}

function compact (obj) {
  Object.keys(obj).forEach((key) => {
    if (obj[key] && typeof obj[key] === 'object') compact(obj[key])
    else if (obj[key] == null) delete obj[key]
  })
}

function isValid (entry) {
 return entry.address
    && crypto.isKey(entry.address)
    && !entry.encryptionKey || crypto.isKey(entry.encryptionKey)
    && !entry.name || isName(entry.name)
}
